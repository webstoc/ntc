##Drupal NTC Views Module

This modules integrates the NTRP registration system with Drupal on nnlm.gov.

It creates views that can access data in the NTRP database

It creates registration forms, and other pages for NTRP purposes.
The following URL's are added to drupal through this module:

ntc/classes/site.html
ntc/classes/class_details.html
ntc/classes/confirm.html
ntc/classes/register.html
evaluation/workshops/register.html
mcr/education/register.html
gmr/training/register.html
mar/training/register.html
ner/training/register.html
pnr/training/register.html
psr/training/register.html
scr/training/register.html
sea/training/register.html
training-schedule/
gmr/training-schedule/
mar/training-schedule/
mcr/training-schedule/
ner/training-schedule/
pnr/training-schedule/
psr/training-schedule/
scr/training-schedule/
sea/training-schedule/