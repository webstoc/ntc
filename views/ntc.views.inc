<?php
// $Id$

/**
 * Implements hook_views_pre_view
 * main purpose here is to stop the page from caching
 */
function ntc_views_pre_view(){
    drupal_page_is_cacheable(FALSE);
}

/**
 * Implements hook_views_data
 * Allows views integration for the NTC database.  This is the meat and potatoes of
 * describing external data to Drupal
 *
 * @return {a.array} an array of table information, keyed by the name of the table
 */
function ntc_views_data(){
    if (variable_get('nnlm_enable_ntc_database_views') === FALSE) {
        return;
    }
    $data                                         = array();
    //class descriptions table definition
    $data['class_schedules']['table']['group']    = t('NTC');
    $data['class_schedules']['table']['title']    = t('NTRP schedules');
    $data['class_schedules']['table']['help']     = t("Contains class schedules");
    $data['class_schedules']['table']['base']     = array(
        'field' => 'schedule_id',
        'title' => t('NTRP Scheduled Classes'),
        'help' => t('Class Schedule information from NTRP'),
        'database' => 'general'
    );
    //class schedules table definition
    $data['class_descriptions']['table']['group'] = t('NTC');
    $data['class_descriptions']['table']['title'] = t('NTRP class descriptions');
    $data['class_descriptions']['table']['help']  = t('Contains class info');
    
    //class regions table definition
    $data['class_regions']['table']['group'] = t('NTC');
    $data['class_regions']['table']['title'] = t('NTRP regional settings');
    $data['class_regions']['table']['help']  = t('Contains regional settings in NTRP');
    
    //sites definition
    $data['sites']['table']['group'] = t('NTC');
    $data['sites']['table']['title'] = t('NTRP Sites');
    $data['sites']['table']['help']  = t('Contains Site data');
    
    //join condition for table
    $data['class_descriptions']['table']['join']['class_schedules'] = array(
        'left_field' => 'class_id', //column in this table (class_schedules)
        'field' => 'class_id' //column in other table (class_descriptions)
    );
    $data['class_regions']['table']['join']['class_schedules']      = array(
        'left_field' => 'region', //column in this table (class_schedules)
        'field' => 'region_code' //column in other table (class_regions)
    );
    $data['sites']['table']['join']['class_schedules']              = array(
        'left_field' => 'site_id', //column in this table (class_schedules)
        'field' => 'site_id' //column in other table (sites)
    );
    
    /** FIELD DEFINITIONS FOR NTRP data **/
    $defaults = array(
        'numeric_field' => array(
            'field' => array(
                'handler' => 'views_handler_field_numeric',
                'click sortable' => TRUE
            ),
            'argument' => array(
                'handler' => 'views_handler_argument_numeric',
                // the field to display in the summary.
                'name field' => 'title',
                'numeric' => TRUE
            ),
            'filter' => array(
                'handler' => 'views_handler_filter_numeric'
            ),
            'sort' => array(
                'handler' => 'views_handler_sort'
            )
        ),
		'binary_field' => array(
            'field' => array(
                'handler' => 'views_handler_field_numeric',
                'click sortable' => TRUE
            ),
            'argument' => array(
                'handler' => 'views_handler_argument_numeric',
                // the field to display in the summary.
                'name field' => 'title',
                'numeric' => TRUE
            ),
            'filter' => array(
                'handler' => 'views_handler_filter_ntc_bitwise'
            ),
            'sort' => array(
                'handler' => 'views_handler_sort'
            )
        ),
        'string_field' => array(
            'field' => array(
                'handler' => 'views_handler_field_node',
                'click sortable' => TRUE
            ),
            'argument' => array(
                'handler' => 'views_handler_argument_string',
                // the field to display in the summary.
                'name field' => 'title',
                'numeric' => FALSE
            ),
            'filter' => array(
                'handler' => 'views_handler_filter_string'
            ),
            'sort' => array(
                'handler' => 'views_handler_sort'
            )
        ),
        'unix_timestamp_field' => array(
            'field' => array(
                'handler' => 'views_handler_field_date',
                'click sortable' => TRUE
            ),
            'argument' => array(
                'handler' => 'views_handler_argument_string',
                // the field to display in the summary.
                'name field' => 'title',
                'numeric' => FALSE
            ),
            'filter' => array(
                'handler' => 'views_handler_filter_date'
            ),
            'sort' => array(
                'handler' => 'views_handler_sort'
            )
        ),
        'class_date_field' => array(
            'field' => array(
                'handler' => 'views_handler_ntc_date_field',
                'click sortable' => TRUE
            ),
            'argument' => array(
                'handler' => 'views_handler_argument_string',
                // the field to display in the summary.
                'name field' => 'title',
                'numeric' => FALSE
            ),
            'filter' => array(
                'handler' => 'views_handler_filter_string'
            ),
            'sort' => array(
                'handler' => 'views_handler_sort'
            )
        ),
        'class_name_field' => array(
            'field' => array(
                'handler' => 'views_handler_ntc_class_field',
                'click sortable' => TRUE
            ),
            'argument' => array(
                'handler' => 'views_handler_argument_string',
                // the field to display in the summary.
                'name field' => 'title',
                'numeric' => FALSE
            ),
            'filter' => array(
                'handler' => 'views_handler_filter_string'
            ),
            'sort' => array(
                'handler' => 'views_handler_sort'
            )
        ),
        'class_site_field' => array(
            'field' => array(
                'handler' => 'views_handler_ntc_site_field',
                'click sortable' => TRUE
            ),
            'argument' => array(
                'handler' => 'views_handler_argument_string',
                // the field to display in the summary.
                'name field' => 'title',
                'numeric' => FALSE
            ),
            'filter' => array(
                'handler' => 'views_handler_filter_string'
            ),
            'sort' => array(
                'handler' => 'views_handler_sort'
            )
        ),
        'register_field' => array(
            'field' => array(
                'handler' => 'views_handler_ntc_register_field',
                'click sortable' => TRUE
            ),
            'argument' => array(
                'handler' => 'views_handler_argument_string',
                // the field to display in the summary.
                'name field' => 'title',
                'numeric' => FALSE
            ),
            'filter' => array(
                'handler' => 'views_handler_filter_string'
            ),
            'sort' => array(
                'handler' => 'views_handler_sort'
            )
        )
    );
    
    $data['class_schedules']['schedule_id'] = array(
        'title' => t('Schedule ID'),
        'help' => t('The schedule id for this instance of the class')
    ) + $defaults['numeric_field'];
    
    $data['class_schedules']['class_id'] = array(
        'title' => t('Class ID'),
        'help' => t('The class id of this class')
    ) + $defaults['numeric_field'];
    
    $data['class_descriptions']['class_id'] = array(
        'title' => t('Class ID'),
        'help' => t('The class_id from the class description')
    ) + $defaults['numeric_field'];
    
    $data['class_descriptions']['class_name'] = array(
        'title' => t('Class Name'),
        'help' => t('The English name of the class')
    ) + $defaults['string_field'];
	
	$data['class_schedules']['subtitle'] = array(
        'title' => t('Class Subtitle'),
        'help' => t('The Subtitle of the class')
    ) + $defaults['string_field'];
    
	$data['class_schedules']['addtl_class_info'] = array(
        'title' => t('Additional Class Info'),
        'help' => t('Additional information about the class')
    ) + $defaults['string_field'];
	
    $data['class_descriptions']['duration'] = array(
        'title' => t('Class Length'),
        'help' => t('The duration of the class')
    ) + $defaults['string_field'];
    
    $data['class_schedules']['site_id'] = array(
        'title' => t('Site ID'),
        'help' => t('The site id for this class')
    ) + $defaults['numeric_field'];
    
    $data['sites']['city'] = array(
        'title' => t('City'),
        'help' => t('The name of the City')
    ) + $defaults['string_field'];
    
    $data['sites']['state'] = array(
        'title' => t('State'),
        'help' => t('The 2-character Abbreviation for the state')
    ) + $defaults['string_field'];
    
    $data['class_schedules']['status'] = array(
        'title' => t('Class Status'),
        'help' => t('The 1-Letter status code for this class')
    ) + $defaults['string_field'];
    
    $data['class_schedules']['class_date'] = array(
        'title' => t('Class date'),
        'help' => t('The date of the class')
    ) + $defaults['string_field'];
    
    $data['class_schedules']['class_time'] = array(
        'title' => t('Class time'),
        'help' => t('The time of the class')
    ) + $defaults['string_field'];
    
    $data['class_schedules']['end_date'] = array(
        'title' => t('Class End Date'),
        'help' => t('The ending date of the class')
    ) + $defaults['string_field'];
    
    $data['class_schedules']['time_zone'] = array(
        'title' => t('Time Zone'),
        'help' => t('The timezone for this class')
    ) + $defaults['string_field'];
    
    $data['class_schedules']['addtl_date_info'] = array(
        'title' => t('Additional Date Info'),
        'help' => t('Extra details for this class\'s date field')
    ) + $defaults['string_field'];
    
    $data['class_schedules']['start_timestamp_unix'] = array(
        'title' => t('Class Start Date'),
        'help' => t('The start date of the class, formatted as Drupal expects it.')
    ) + $defaults['unix_timestamp_field'];
    
    $data['class_schedules']['region'] = array(
        'title' => t('Class Region'),
        'help' => t('The 3-Letter Region sponsoring this class')
    ) + $defaults['string_field'];
    
    $data['class_regions']['description'] = array(
        'title' => t('Region Name'),
        'help' => t('The full name of the region')
    ) + $defaults['string_field'];
    
    $data['class_regions']['home_url'] = array(
        'title' => t('Region Home'),
        'help' => t('The URL for the region\'s home page')
    ) + $defaults['string_field'];
    
    $data['class_regions']['register_url'] = array(
        'title' => t('Registration URL'),
        'help' => t('The URL for the region\'s registration page')
    ) + $defaults['string_field'];
    
    $data['class_regions']['states'] = array(
        'title' => t('Region\'s States'),
        'help' => t('List of all of the sponsoring Region\'s states')
    ) + $defaults['string_field'];
	
	$data['class_schedules']['cross_region'] = array(
        'title' => t('Cross_region setting'),
        'help' => t('Binary representation of different regions that should show this class.')
    ) + $defaults['binary_field'];
	
    $data['class_schedules']['class_region'] = array(
        'title' => t('Physical region of class'),
        'help' => t('Where the class will be taught')
    ) + $defaults['string_field'];
    
    $data['class_schedules']['other_reg_info'] = array(
        'title' => t('Additional Registration Info'),
        'help' => t('Extra details for registering for this class')
    ) + $defaults['string_field'];
    
    $data['class_schedules']['instructor'] = array(
        'title' => t('Class Instructor'),
        'help' => t('The instructor for this instance of the class')
    ) + $defaults['string_field'];
    
    $data['class_schedules']['reg_required'] = array(
        'title' => t('Registration required'),
        'help' => t('Whether or not registration is required')
    ) + $defaults['numeric_field'];
    
    $data['class_schedules']['external_reg_link'] = array(
        'title' => t('External Registration Link'),
        'help' => t('The external registration link for this class')
    ) + $defaults['string_field'];
    
    $data['class_schedules']['start_timestamp'] = array(
        'title' => t('Complete Date Field'),
        'help' => t('Combines several fields to logically determine the date field. Including a day of the week, date, time, and any other details.')
    ) + $defaults['class_date_field'];
    
    $data['class_descriptions']['complete_class_description'] = array(
        'title' => t('Complete Class Description Field'),
        'help' => t('Includes the class name, duration, and link to class details.')
    ) + $defaults['class_name_field'];
    
    $data['sites']['complete_site'] = array(
        'title' => t('Complete Site Field'),
        'help' => t('Displays the City, ST of a site, or Online for online classes. Appropriate hyperlinks are used.')
    ) + $defaults['class_site_field'];
    
    $data['class_schedules']['complete_register'] = array(
        'title' => t('Complete Register Field'),
        'help' => t('Combines several fields to logically determine the registration field. Including a button, and any other details.')
    ) + $defaults['register_field'];
    
    return $data;
}