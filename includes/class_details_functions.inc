<?php
/**
 * @file
 * Custom functions for handling ntrp class_details pages
 *
 * Place this code in
 * /sites/all/ntc/includes/class_details_functions.inc
 */

/**
 * returns an associative array that drupal can use to create a page, or redirects if neccesary
 */
function get_class_details_page(){
	drupal_page_is_cacheable(FALSE);
	if(!isset($_GET['class_id'])){
		drupal_set_message(t('The page requested does not exist'),'error');
		drupal_goto('/training-schedule');
	}if($class_info = get_class_details($_GET['class_id'])){
		if($class_info['description_url']){
			drupal_goto($class_info['description_url']);
		}else{
			$html = build_html_for_class($class_info);
		}
	}else{
		return;
	}
	$page[] = array(
		'#type' => 'item',
		'#markup' => t($html)
	);
	$classes_view = get_classes_view($class_info['class_name']);
	$page['upcoming_classes']['#markup'] = $classes_view->render();
	return $page;
}

/**
 * Returns the html to be used to display the class details.
 */
function build_html_for_class($class_info){
	$html = '';
	$html .= '<h1>'.$class_info['class_name'].'</h1>';
	if(!open_schedules_exist_or_redirect($class_info['class_id'])){
		$html .='<p>There are no sessions currently scheduled for this class, the following information may be out of date.</p>';		
	}
	$html.=($class_info['duration']?'<p><strong>'.$class_info['duration'].'</strong></p>':'');
	$html.=($class_info['description']?'<h2>Class Description:</h2><p>'.$class_info['description'].'</p>':'');
	$html.=($class_info['agenda']?'<h2>Agenda:</h2><p>'.$class_info['agenda'].'</p>':'');
	$html.='<hr/>';
	return $html;
}

/**
 * Returns True if there is at least one scheduled class with the specified class_id number. 
 * IF, there is not one for the specified class_id number, but there is one for that class_id's class_name ( like maybe it is
 * being taught by another region), we will redirect them to that listing to avoid any confusion.
 * I acknowledge while writing this that this might lead to some confusion internally. 
 * at this point I don't care, but if it does become an issue, my currently recommended course of action would be
 * to add a variable to the url in the links generated on the back end that go to this page with something like:
 * class_details.html?class_id=x&show=1
 * which would make the redirect not happen. I'm not gonna worry about it right now though.
 */
function open_schedules_exist_or_redirect($class_id){
	$class_info = get_class_details($class_id);
	db_set_active('general');
		
	$res = db_select('class_schedules','cs')
		->fields('cs')
		->condition('class_id',$class_id,'=')
		->condition('status','X','!=')
		->condition('status','C','!=')
		->execute();
	if($res->fetchAssoc()){
		db_set_active();
		return true;
	}else{
		$query = db_select('class_descriptions','cd')
			-> fields('cd')
			-> condition('class_name',$class_info['class_name'],'=')
			-> condition('class_id',$class_id,'!=');
		$res = $query-> execute();
		while($row = $res->fetchAssoc()){
			$query = db_select('class_schedules','cs')
				->fields('cs')
				->condition('class_id',$row['class_id'],'=')
				->condition('status','X','!=')
				->condition('status','C','!=');
			$result = $query->execute();
			if($result->fetchAssoc()){
				db_set_active();
				$options = array('query'=>array('class_id'=>$row['class_id']));
				drupal_goto(current_path(),$options);//if there is a 'current' class_description elsewhere, go there.
			}
		}
		db_set_active();
		return false;
	}
}

/**
* Get view for upcoming opportunities
*/
function get_classes_view($class_name){	
	$view = new view();
	$view->name = 'class_details';
	$view->description = '';
	$view->tag = 'default';
	$view->base_table = 'class_schedules';
	$view->human_name = 'class_details';
	$view->core = 7;
	$view->api_version = '3.0';
	$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
	
	/* Display: Master */
	$handler = $view->new_display('default', 'Master', 'default');
	$handler->display->display_options['title'] = 'class_details';
	$handler->display->display_options['css_class'] = 'ntrp-schedule';
	$handler->display->display_options['use_more_always'] = FALSE;
	$handler->display->display_options['access']['type'] = 'none';
	$handler->display->display_options['cache']['type'] = 'none';
	$handler->display->display_options['query']['type'] = 'views_query';
	$handler->display->display_options['exposed_form']['type'] = 'basic';
	$handler->display->display_options['pager']['type'] = 'full';
	$handler->display->display_options['pager']['options']['items_per_page'] = '100';
	$handler->display->display_options['style_plugin'] = 'table';
	$handler->display->display_options['style_options']['caption'] = 'Current Training Opportunities for this Class:';
	$handler->display->display_options['style_options']['summary'] = 'Current Training Opportunities for this Class:';
	/* No results behavior: Global: Unfiltered text */
	$handler->display->display_options['empty']['area_text_custom']['id'] = 'area_text_custom';
	$handler->display->display_options['empty']['area_text_custom']['table'] = 'views';
	$handler->display->display_options['empty']['area_text_custom']['field'] = 'area_text_custom';
	$handler->display->display_options['empty']['area_text_custom']['label'] = 'No Classes Found';
	$handler->display->display_options['empty']['area_text_custom']['empty'] = TRUE;
	$handler->display->display_options['empty']['area_text_custom']['content'] = '<p>There are no available sessions of this class</p>';
	/* Field: NTC: Schedule ID */
	$handler->display->display_options['fields']['schedule_id']['id'] = 'schedule_id';
	$handler->display->display_options['fields']['schedule_id']['table'] = 'class_schedules';
	$handler->display->display_options['fields']['schedule_id']['field'] = 'schedule_id';
	$handler->display->display_options['fields']['schedule_id']['exclude'] = TRUE;
	/* Field: NTC: Additional Date Info */
	$handler->display->display_options['fields']['addtl_date_info']['id'] = 'addtl_date_info';
	$handler->display->display_options['fields']['addtl_date_info']['table'] = 'class_schedules';
	$handler->display->display_options['fields']['addtl_date_info']['field'] = 'addtl_date_info';
	$handler->display->display_options['fields']['addtl_date_info']['exclude'] = TRUE;
	/* Field: NTC: Additional Registration Info */
	$handler->display->display_options['fields']['other_reg_info']['id'] = 'other_reg_info';
	$handler->display->display_options['fields']['other_reg_info']['table'] = 'class_schedules';
	$handler->display->display_options['fields']['other_reg_info']['field'] = 'other_reg_info';
	$handler->display->display_options['fields']['other_reg_info']['exclude'] = TRUE;
	/* Field: NTC: City */
	$handler->display->display_options['fields']['city']['id'] = 'city';
	$handler->display->display_options['fields']['city']['table'] = 'sites';
	$handler->display->display_options['fields']['city']['field'] = 'city';
	$handler->display->display_options['fields']['city']['exclude'] = TRUE;
	/* Field: NTC: Class date */
	$handler->display->display_options['fields']['class_date']['id'] = 'class_date';
	$handler->display->display_options['fields']['class_date']['table'] = 'class_schedules';
	$handler->display->display_options['fields']['class_date']['field'] = 'class_date';
	$handler->display->display_options['fields']['class_date']['exclude'] = TRUE;
	/* Field: NTC: Class End Date */
	$handler->display->display_options['fields']['end_date']['id'] = 'end_date';
	$handler->display->display_options['fields']['end_date']['table'] = 'class_schedules';
	$handler->display->display_options['fields']['end_date']['field'] = 'end_date';
	$handler->display->display_options['fields']['end_date']['exclude'] = TRUE;
	/* Field: NTC: Class ID */
	$handler->display->display_options['fields']['class_id']['id'] = 'class_id';
	$handler->display->display_options['fields']['class_id']['table'] = 'class_schedules';
	$handler->display->display_options['fields']['class_id']['field'] = 'class_id';
	$handler->display->display_options['fields']['class_id']['exclude'] = TRUE;
	/* Field: NTC: Class ID */
	$handler->display->display_options['fields']['class_id_1']['id'] = 'class_id_1';
	$handler->display->display_options['fields']['class_id_1']['table'] = 'class_descriptions';
	$handler->display->display_options['fields']['class_id_1']['field'] = 'class_id';
	$handler->display->display_options['fields']['class_id_1']['exclude'] = TRUE;
	/* Field: NTC: Class Subtitle */
	$handler->display->display_options['fields']['subtitle']['id'] = 'subtitle';
	$handler->display->display_options['fields']['subtitle']['table'] = 'class_schedules';
	$handler->display->display_options['fields']['subtitle']['field'] = 'subtitle';
	$handler->display->display_options['fields']['subtitle']['label'] = '';
	$handler->display->display_options['fields']['subtitle']['exclude'] = TRUE;
	$handler->display->display_options['fields']['subtitle']['element_label_colon'] = FALSE;
	/* Field: NTC: Additional Class Info */
	$handler->display->display_options['fields']['addtl_class_info']['id'] = 'addtl_class_info';
	$handler->display->display_options['fields']['addtl_class_info']['table'] = 'class_schedules';
	$handler->display->display_options['fields']['addtl_class_info']['field'] = 'addtl_class_info';
	$handler->display->display_options['fields']['addtl_class_info']['label'] = '';
	$handler->display->display_options['fields']['addtl_class_info']['exclude'] = TRUE;
	$handler->display->display_options['fields']['addtl_class_info']['element_label_colon'] = FALSE;	
	/* Field: NTC: Class Instructor */
	$handler->display->display_options['fields']['instructor']['id'] = 'instructor';
	$handler->display->display_options['fields']['instructor']['table'] = 'class_schedules';
	$handler->display->display_options['fields']['instructor']['field'] = 'instructor';
	$handler->display->display_options['fields']['instructor']['exclude'] = TRUE;
	/* Field: NTC: Class Length */
	$handler->display->display_options['fields']['duration']['id'] = 'duration';
	$handler->display->display_options['fields']['duration']['table'] = 'class_descriptions';
	$handler->display->display_options['fields']['duration']['field'] = 'duration';
	$handler->display->display_options['fields']['duration']['exclude'] = TRUE;
	/* Field: NTC: Class Name */
	$handler->display->display_options['fields']['class_name']['id'] = 'class_name';
	$handler->display->display_options['fields']['class_name']['table'] = 'class_descriptions';
	$handler->display->display_options['fields']['class_name']['field'] = 'class_name';
	$handler->display->display_options['fields']['class_name']['exclude'] = TRUE;
	/* Field: NTC: Class Start Date */
	$handler->display->display_options['fields']['start_timestamp_unix']['id'] = 'start_timestamp_unix';
	$handler->display->display_options['fields']['start_timestamp_unix']['table'] = 'class_schedules';
	$handler->display->display_options['fields']['start_timestamp_unix']['field'] = 'start_timestamp_unix';
	$handler->display->display_options['fields']['start_timestamp_unix']['exclude'] = TRUE;
	$handler->display->display_options['fields']['start_timestamp_unix']['date_format'] = 'long';
	$handler->display->display_options['fields']['start_timestamp_unix']['second_date_format'] = 'long';
	/* Field: NTC: Class Status */
	$handler->display->display_options['fields']['status']['id'] = 'status';
	$handler->display->display_options['fields']['status']['table'] = 'class_schedules';
	$handler->display->display_options['fields']['status']['field'] = 'status';
	$handler->display->display_options['fields']['status']['exclude'] = TRUE;
	/* Field: NTC: Class time */
	$handler->display->display_options['fields']['class_time']['id'] = 'class_time';
	$handler->display->display_options['fields']['class_time']['table'] = 'class_schedules';
	$handler->display->display_options['fields']['class_time']['field'] = 'class_time';
	$handler->display->display_options['fields']['class_time']['exclude'] = TRUE;
	/* Field: NTC: External Registration Link */
	$handler->display->display_options['fields']['external_reg_link']['id'] = 'external_reg_link';
	$handler->display->display_options['fields']['external_reg_link']['table'] = 'class_schedules';
	$handler->display->display_options['fields']['external_reg_link']['field'] = 'external_reg_link';
	$handler->display->display_options['fields']['external_reg_link']['exclude'] = TRUE;
	/* Field: NTC: Physical region of class */
	$handler->display->display_options['fields']['class_region']['id'] = 'class_region';
	$handler->display->display_options['fields']['class_region']['table'] = 'class_schedules';
	$handler->display->display_options['fields']['class_region']['field'] = 'class_region';
	$handler->display->display_options['fields']['class_region']['exclude'] = TRUE;
	/* Field: NTC: Region Home */
	$handler->display->display_options['fields']['home_url']['id'] = 'home_url';
	$handler->display->display_options['fields']['home_url']['table'] = 'class_regions';
	$handler->display->display_options['fields']['home_url']['field'] = 'home_url';
	$handler->display->display_options['fields']['home_url']['exclude'] = TRUE;
	/* Field: NTC: Region Name */
	$handler->display->display_options['fields']['description']['id'] = 'description';
	$handler->display->display_options['fields']['description']['table'] = 'class_regions';
	$handler->display->display_options['fields']['description']['field'] = 'description';
	$handler->display->display_options['fields']['description']['exclude'] = TRUE;
	/* Field: NTC: Region's States */
	$handler->display->display_options['fields']['states']['id'] = 'states';
	$handler->display->display_options['fields']['states']['table'] = 'class_regions';
	$handler->display->display_options['fields']['states']['field'] = 'states';
	$handler->display->display_options['fields']['states']['exclude'] = TRUE;
	/* Field: NTC: Registration required */
	$handler->display->display_options['fields']['reg_required']['id'] = 'reg_required';
	$handler->display->display_options['fields']['reg_required']['table'] = 'class_schedules';
	$handler->display->display_options['fields']['reg_required']['field'] = 'reg_required';
	$handler->display->display_options['fields']['reg_required']['exclude'] = TRUE;
	/* Field: NTC: Registration URL */
	$handler->display->display_options['fields']['register_url']['id'] = 'register_url';
	$handler->display->display_options['fields']['register_url']['table'] = 'class_regions';
	$handler->display->display_options['fields']['register_url']['field'] = 'register_url';
	$handler->display->display_options['fields']['register_url']['exclude'] = TRUE;
	/* Field: NTC: Site ID */
	$handler->display->display_options['fields']['site_id']['id'] = 'site_id';
	$handler->display->display_options['fields']['site_id']['table'] = 'class_schedules';
	$handler->display->display_options['fields']['site_id']['field'] = 'site_id';
	$handler->display->display_options['fields']['site_id']['exclude'] = TRUE;
	/* Field: NTC: State */
	$handler->display->display_options['fields']['state']['id'] = 'state';
	$handler->display->display_options['fields']['state']['table'] = 'sites';
	$handler->display->display_options['fields']['state']['field'] = 'state';
	$handler->display->display_options['fields']['state']['exclude'] = TRUE;
	/* Field: NTC: Time Zone */
	$handler->display->display_options['fields']['time_zone']['id'] = 'time_zone';
	$handler->display->display_options['fields']['time_zone']['table'] = 'class_schedules';
	$handler->display->display_options['fields']['time_zone']['field'] = 'time_zone';
	$handler->display->display_options['fields']['time_zone']['exclude'] = TRUE;
	/* Field: NTC: Complete Date Field */
	$handler->display->display_options['fields']['start_timestamp']['id'] = 'start_timestamp';
	$handler->display->display_options['fields']['start_timestamp']['table'] = 'class_schedules';
	$handler->display->display_options['fields']['start_timestamp']['field'] = 'start_timestamp';
	$handler->display->display_options['fields']['start_timestamp']['label'] = 'Date';
	$handler->display->display_options['fields']['start_timestamp']['element_class'] = 'col-w-25';
	/* Field: NTC: Complete Class Description Field */
	$handler->display->display_options['fields']['complete_class_description']['id'] = 'complete_class_description';
	$handler->display->display_options['fields']['complete_class_description']['table'] = 'class_descriptions';
	$handler->display->display_options['fields']['complete_class_description']['field'] = 'complete_class_description';
	$handler->display->display_options['fields']['complete_class_description']['label'] = 'Class Name';
	$handler->display->display_options['fields']['complete_class_description']['element_class'] = 'col-w-30';
	/* Field: NTC: Complete Site Field */
	$handler->display->display_options['fields']['complete_site']['id'] = 'complete_site';
	$handler->display->display_options['fields']['complete_site']['table'] = 'sites';
	$handler->display->display_options['fields']['complete_site']['field'] = 'complete_site';
	$handler->display->display_options['fields']['complete_site']['label'] = 'Site';
	$handler->display->display_options['fields']['complete_site']['element_class'] = 'col-w-15';
	/* Field: NTC: Class Region */
	$handler->display->display_options['fields']['region']['id'] = 'region';
	$handler->display->display_options['fields']['region']['table'] = 'class_schedules';
	$handler->display->display_options['fields']['region']['field'] = 'region';
	$handler->display->display_options['fields']['region']['label'] = 'Sponsoring RML/Center';
	$handler->display->display_options['fields']['region']['alter']['alter_text'] = TRUE;
	$handler->display->display_options['fields']['region']['alter']['text'] = '<a href="[home_url]">[description]</a>';
	$handler->display->display_options['fields']['region']['element_class'] = 'col-w-15';
	/* Field: NTC: Complete Register Field */
	$handler->display->display_options['fields']['complete_register']['id'] = 'complete_register';
	$handler->display->display_options['fields']['complete_register']['table'] = 'class_schedules';
	$handler->display->display_options['fields']['complete_register']['field'] = 'complete_register';
	$handler->display->display_options['fields']['complete_register']['label'] = 'Register';
	$handler->display->display_options['fields']['complete_register']['element_class'] = 'col-w-15';
	/* Filter criterion: NTC: Class Status */
	$handler->display->display_options['filters']['status']['id'] = 'status';
	$handler->display->display_options['filters']['status']['table'] = 'class_schedules';
	$handler->display->display_options['filters']['status']['field'] = 'status';
	$handler->display->display_options['filters']['status']['operator'] = '!=';
	$handler->display->display_options['filters']['status']['value'] = 'C';
	/* Filter criterion: NTC: Class Name */
	$handler->display->display_options['filters']['class_name']['id'] = 'class_name';
	$handler->display->display_options['filters']['class_name']['table'] = 'class_descriptions';
	$handler->display->display_options['filters']['class_name']['field'] = 'class_name';
	$handler->display->display_options['filters']['class_name']['value'] = $class_name;
	/* Filter criterion: NTC: Class Status */
	$handler->display->display_options['filters']['status_1']['id'] = 'status_1';
	$handler->display->display_options['filters']['status_1']['table'] = 'class_schedules';
	$handler->display->display_options['filters']['status_1']['field'] = 'status';
	$handler->display->display_options['filters']['status_1']['operator'] = '!=';
	$handler->display->display_options['filters']['status_1']['value'] = 'X';
	
	return $view;	
}