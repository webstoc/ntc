<?php
/**
 * @file
 * Custom views handler definition.
 *
 * Place this code in
 * /sites/all/[custom_module_name]/includes/views_handler_my_custom_field.inc
 */

/**
 * Custom handler class.
 *
 * @ingroup views_field_handlers
 */
class views_handler_ntc_class_field extends views_handler_field{
	/**
	 * {@inheritdoc}
	 *
	 * Perform any database or cache data retrieval here. In this example there is
	 * none.
	 */
	function query(){
		
	}
	
	/**
	 * Called to determine what to tell the clicksorter.
	 * can't use the default sorter because query() is blank above.
	 */
	function click_sort($order){
		$params = $this->options['group_type'] != 'group' ? array(
			'function' => $this->options['group_type']
		) : array();
		$this->query->add_orderby(NULL, NULL, $order, "class_descriptions.class_name", $params);
	}
	
	/**
	 * {@inheritdoc}
	 *
	 * Modify any end user views settings here. Debug $options to view the field
	 * settings you can change.
	 */
	function option_definition(){
		$options = parent::option_definition();
		return $options;
	}
	
	/**
	 * {@inheritdoc}
	 *
	 * Make changes to the field settings form seen by the end user when adding
	 * your field.
	 */
	function options_form(&$form, &$form_state){
		parent::options_form($form, $form_state);
	}
	
	/**
	 * Render callback handler.
	 *
	 * Return the markup that will appear in the rendered field.
	 */
	function render($values){
		$class_name = ($values->class_schedules_subtitle==null?$values->class_descriptions_class_name:$values->class_descriptions_class_name.': '.$values->class_schedules_subtitle);
		$class_link = '/ntc/classes/class_details.html?class_id=' . $values->class_schedules_class_id;
		$result = '<a href="'.$class_link.'">' . $class_name . '</a><br/>'.
			($values->class_schedules_addtl_class_info==null?'':$values->class_schedules_addtl_class_info.'<br/>').
			($values->class_descriptions_duration==null?'':'('.$values->class_descriptions_duration.')<br/>').
			'<a href="'.$class_link.'">Details</a>';
		return $result;
	}
}