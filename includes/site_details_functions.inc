<?php
/**
 * @file
 * Custom functions for handling ntrp site pages
 *
 * Place this code in
 * /sites/all/ntc/includes/site_details_functions.inc
 */
 

/**
 * returns an associative array that drupal can use to create a page, or redirects if neccesary
 */
function get_site_details_page(){
	drupal_page_is_cacheable(FALSE);
	if(!isset($_GET['site_id'])){
		drupal_set_message(t('The page requested does not exist'),'error');
		drupal_goto('/training-schedule');
	}if($site_info = get_site_details($_GET['site_id'])){
		$html = build_html_for_site($site_info);
	}else{
		return;
	}
	$page = array(
		'#type' => 'item',
		'#markup' => t($html)
	);
	return $page;
}

function get_site_details($site_id){
	db_set_active('general');
	$query = db_select('sites','s')
		->fields('s')
		->condition('site_id',$site_id,'=');
	$res = $query->execute();
	if($row = $res->fetchAssoc()){
		db_set_active();
		return $row;
	}else{
		db_set_active();
		drupal_set_message(t('The page requested does not exist'),'error');
		drupal_goto('/training-schedule');
		return false;
	}			
}

function get_hotel_html($site_id){
	db_set_active('general');
	$query = db_select('hotels','h')
		->fields('h')
		->condition('site_id',$site_id,'=');
	$res = $query->execute();
	$html = false;
	while($row = $res->fetchAssoc()){
		if($html == false){
			$html='<div style="padding-left: 3em">';
		}
		$html.=	'<p><strong>'.
			($row['hotel_link']?'<a href="'.$row['hotel_link'].'">':'').
			($row['hotel_name']?$row['hotel_name']:'').
			($row['hotel_link']?'</a>':'').
			'</strong>'.
			($row['h_address1']?'<br/>'.$row['h_address1']:'').
			($row['h_address2']?'<br/>'.$row['h_address2']:'').
			($row['h_city']?'<br/>'.$row['h_city'].', '.$row['h_state'].' '.$row['h_zip']:'').
			($row['h_phone']?'<br/>Phone: '.$row['h_phone']:'').
			($row['h_fax']?'<br/>Fax: '.$row['h_fax']:'').
			($row['h_email']?'<br/>email: <a href="mailto:'.$row['h_email'].'>'.$row['h_email'].'</a>':'').
			($row['hotel_note']?'<br/>'.$row['hotel_note']:'').
			'</p>';
	}
	if($html){
		$html.='</div>';
	}
	db_set_active();
	return $html;
	
}

function build_html_for_site($site){
	$html = '<div><h1>Attending classes in '.$site['city'].', '.$site['state'].' </h1>';
	if($site["contact_name"]!==null){
		$html.='<div style="float:right; width:38%"><strong>Local Contact:</strong><br/>';
		$html.=($site['contact_name']?$site['contact_name']:'');
		$html.=($site['contact_email']?'<br/><a href="mailto:'.$site['contact_email'].'">'.$site['contact_email'].'</a>':'');
		$html.=($site['contact_phone']?'<br/>'.$site['contact_phone']:'');
		$html.='</div>';
	}
	$html.='<h3>Classes will be held at:</h3>'.
		'<div style="display:inline; overflow:auto;"><div style="padding-left: 3em"><p><strong>'.
		($site['organization_link']?'<a href="'.$site['organization_link'].'">':'').
			$site['organization'].
		($site['organization_link']?'</a>':'').'</strong>';
	if($site['class_location']){
		$html .='<br/>'.
		($site['location_link']?'<a href="'.$site['organization_link'].'">':'').
			'<strong>'.$site['class_location'].'</strong>'.
		($site['location_link']?'</a>':'');
	}
	$html.=($site['address1']?'<br/>'.$site['address1']:'');
	$html.=($site['address2']?'<br/>'.$site['address2']:'');
	$html.=($site['add_city']?'<br/>'.$site['add_city'].', '.$site['add_state'].' '.$site['zip']:'');
	$html.='</p>';
	if($site['map_link']||$site['directions_link']||$site['parking_link']){
		$html.='<ul>';
		$html.=($site['map_link']?'<li><a href="'.$site['map_link'].'">Map</a></li>':'');
		$html.=($site['directions_link']?'<li><a href="'.$site['directions_link'].'">Directions</a></li>':'');
		$html.=($site['parking_link']?'<li><a href="'.$site['parking_link'].'">Parking</a></li>':'');
		$html.='</ul>';
	}
	$html.=($site['direction_notes']?'<p>'.$site['direction_notes'].'</p>':'');
	$html.=($site['miscellaneous']?'<p>'.$site['miscellaneous'].'</p>':'');
	$html.='</div>';
	if(($hotel_html = get_hotel_html($site['site_id']))||$site['hotel_notes']||$site['additional_lodging_link']){
		$html.='<hr/>';
		$html.='<h3>Hotels</h3>';
		$html.=($site['hotel_notes']?'<p>'.$site['hotel_notes'].'</p>':'');
		$html.=($hotel_html?$hotel_html:'');
		$html.=($site['additional_lodging_link']?'<p><a href="'.$site['additional_lodging_link'].'" >Click here</a> for additional hotel addresses and phone numbers.</p>':'');
	}	
	return $html;
}