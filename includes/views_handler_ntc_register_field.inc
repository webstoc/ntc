<?php
/**
 * @file
 * Custom views handler definition.
 *
 * Place this code in
 * /sites/all/[custom_module_name]/includes/views_handler_my_custom_field.inc
 */

/**
 * Custom handler class.
 *
 * @ingroup views_field_handlers
 */
class views_handler_ntc_register_field extends views_handler_field{
	/**
	 * {@inheritdoc}
	 *
	 * Perform any database or cache data retrieval here. In this example there is
	 * none.
	 */
	function query(){
		
	}
	
	/**
	 * Called to determine what to tell the clicksorter.
	 * can't use the default sorter because query() is blank above.
	 */
	function click_sort($order){
		$params = $this->options['group_type'] != 'group' ? array(
			'function' => $this->options['group_type']
		) : array();
		$this->query->add_orderby(NULL, NULL, $order, "FIELD(class_schedules.status,'O','W','L','D')", $params);
	}
	
	/**
	 * {@inheritdoc}
	 *
	 * Modify any end user views settings here. Debug $options to view the field
	 * settings you can change.
	 */
	function option_definition(){
		$options = parent::option_definition();
		return $options;
	}
	
	/**
	 * {@inheritdoc}
	 *
	 * Make changes to the field settings form seen by the end user when adding
	 * your field.
	 */
	function options_form(&$form, &$form_state){
		parent::options_form($form, $form_state);
	}
	
	/**
	 * Render callback handler.
	 *
	 * Return the markup that will appear in the rendered field.
	 */
	function render($values){
		$result = '';
		//if the user can register, Build the link.
		if (($values->class_schedules_status == 'W' || $values->class_schedules_status == 'O') && isset($values->class_schedules_reg_required) && intval($values->class_schedules_reg_required) === 1) {
			$link_text = ($values->class_schedules_status == 'W') ? 'Waiting List' : 'Register'; //default, replace with appropriate.
			if (isset($values->class_schedules_external_reg_link) && !is_null($values->class_schedules_external_reg_link)) {
				if (filter_var($values->class_schedules_external_reg_link, FILTER_VALIDATE_EMAIL)) {
					$registration_url = 'mailto:' . $values->class_schedules_external_reg_link . '&subject=' . preg_replace('/\s/', '%20', $values->class_descriptions_class_name) . '%20-%20' . $values->class_schedules_class_date . ',%20' . preg_replace('/\s/', '%20', $values->sites_city);
				} else {
					$registration_url = $values->class_schedules_external_reg_link;
				}
			} else {
				$registration_url = $values->class_regions_register_url . '?schedule_id=' . $values->schedule_id;
			}
			$result = '<p><a class="ntrp-registration button" href="' . $registration_url . '">' . $link_text . '</a></p>';
		} else { // No link necessary
			switch ($values->class_schedules_status) {
				case 'W': //wait list
				case 'O': //open for registration
					if (empty($values->class_schedules_other_reg_info)) {
						$result = ("<p>Registration not Required.</p>");
					}
					break;
				case 'L': //registration is closed
					$result = ("<p class='error'>Registration is closed for this class</p>");
					break;
				case 'C': //don't bother; these aren't displayed.
					return t("<p class='error'>Closed</p>");
					break;
				case 'D': //cancelled
					return t("<p class='error'>This class has been cancelled</a></p>");
					break;
			}
		}
		if(!empty($values->class_schedules_other_reg_info)){
			$result .= "<p>" . $values->class_schedules_other_reg_info . "</p>";
		}
		return $result;
	}
}
