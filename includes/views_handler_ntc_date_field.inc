<?php
/**
 * @file
 * Custom views handler definition.
 *
 * Place this code in
 * /sites/all/[custom_module_name]/includes/views_handler_my_custom_field.inc
 */

/**
 * Custom handler class.
 *
 * @ingroup views_field_handlers
 */
class views_handler_ntc_date_field extends views_handler_field{
	/**
	 * {@inheritdoc}
	 *
	 * Perform any database or cache data retrieval here. In this example there is
	 * none.
	 */
	function query(){
		
	}
	
	/**
	 * Called to determine what to tell the clicksorter.
	 * can't use the default sorter because query() is blank above.
	 */
	function click_sort($order){
		$params = $this->options['group_type'] != 'group' ? array(
			'function' => $this->options['group_type']
		) : array();
		$this->query->add_orderby(NULL, NULL, $order, $this->real_field, $params);
	}
	
	/**
	 * {@inheritdoc}
	 *
	 * Modify any end user views settings here. Debug $options to view the field
	 * settings you can change.
	 */
	function option_definition(){
		$options = parent::option_definition();
		return $options;
	}
	
	/**
	 * {@inheritdoc}
	 *
	 * Make changes to the field settings form seen by the end user when adding
	 * your field.
	 */
	function options_form(&$form, &$form_state){
		parent::options_form($form, $form_state);
	}
	
	/**
	 * Render callback handler.
	 *
	 * Return the markup that will appear in the rendered field.
	 */
	function render($values){
		$class_date_timestamp = strtotime($values->class_schedules_class_date);
		$dw                   = date("l", $class_date_timestamp);
		$start_date_written   = date("F jS, Y", $class_date_timestamp);
		if ($values->class_schedules_end_date != NULL && $values->class_schedules_end_date != "") {
			$end_date_written = date("F jS, Y", strtotime($values->class_schedules_end_date));
			$result           = $dw . "<br/>" . $start_date_written . " - " . $end_date_written;
		} else if ($values->class_schedules_class_time != NULL && $values->class_schedules_class_time != "") {
			$class_time_written = date("g:i a", strtotime($values->class_schedules_class_time)) . " " . $values->class_schedules_time_zone;
			$result             = $dw . "<br/>" . $start_date_written . "<br/>" . $class_time_written;
		} else {
			$result = $dw . "<br/>" . $start_date_written;
		}
		if ($values->class_schedules_addtl_date_info != NULL && $values->class_schedules_addtl_date_info != "") {
			$result .= "<br/>" . preg_replace("/(<br[ ]*\/?>)+/", "<br/>", nl2br($values->class_schedules_addtl_date_info, true));
		}
		return $result;
	}
}