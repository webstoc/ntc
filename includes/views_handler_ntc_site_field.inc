<?php
/**
 * @file
 * Custom views handler definition.
 *
 * Place this code in
 * /sites/all/[custom_module_name]/includes/views_handler_my_custom_field.inc
 */

/**
 * Custom handler class.
 *
 * @ingroup views_field_handlers
 */
class views_handler_ntc_site_field extends views_handler_field{
	/**
	 * {@inheritdoc}
	 *
	 * Perform any database or cache data retrieval here. In this example there is
	 * none.
	 */
	function query(){
		
	}
	
	/**
	 * Called to determine what to tell the clicksorter.
	 * can't use the default sorter because query() is blank above.
	 */
	function click_sort($order){
		$params = $this->options['group_type'] != 'group' ? array(
			'function' => $this->options['group_type']
		) : array();
		$this->query->add_orderby(NULL, NULL, $order, "sites.state", $params);
	}
	
	/**
	 * {@inheritdoc}
	 *
	 * Modify any end user views settings here. Debug $options to view the field
	 * settings you can change.
	 */
	function option_definition(){
		$options = parent::option_definition();
		return $options;
	}
	
	/**
	 * {@inheritdoc}
	 *
	 * Make changes to the field settings form seen by the end user when adding
	 * your field.
	 */
	function options_form(&$form, &$form_state){
		parent::options_form($form, $form_state);
	}
	
	/**
	 * Render callback handler.
	 *
	 * Return the markup that will appear in the rendered field.
	 */
	function render($values){
		if ($values->sites_state != "XX") {
			$result = "<a href='/ntc/classes/site.html?site_id=" . $values->class_schedules_site_id . "'>" . $values->sites_city . ", " . $values->sites_state . "</a>";
		} else {
			$result = "Online";
		}
		return $result;
	}
}